package com.knackter.demo.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by RUYEE on 2/27/2017.
 */

public class TweetData {


/*
    public TweetData(String load) {
        return ;
    }
*/

    @Override
    public String toString() {
        return "TweetData{" +
                "id=" + id +
                ", content='" + content + '\'' +
                ", creationTime='" + creationTime + '\'' +
                ", likeCount=" + likeCount +
                ", imageUrl='" + imageUrl + '\'' +
                '}';
    }

  /*  public TweetData(String content) {
        this.content = content;
    }*/

    @SerializedName("id")
    @Expose
    public Integer id;
    @SerializedName("content")
    @Expose
    public String content;
    @SerializedName("creationTime")
    @Expose
    private String creationTime;
    @SerializedName("likeCount")
    @Expose
    private Integer likeCount;

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    @SerializedName("imageUrl")
    @Expose
    private String imageUrl;

    /**
     * No args constructor for use in serialization
     *
     * @param load
     */
    public TweetData(String load) {
    }

    public TweetData(String content, String imageUrl) {
        super();
        this.content = content;
        this.imageUrl = imageUrl;
    }
    public TweetData(Integer id, String content, String creationTime, Integer likeCount , String imageUrl) {
        super();
        this.id = id;
        this.content = content;
        this.creationTime = creationTime;
        this.likeCount = likeCount;
        this.imageUrl = imageUrl;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getCreationTime() {
        return creationTime;
    }

    public void setCreationTime(String creationTime) {
        this.creationTime = creationTime;
    }

    public Integer getLikeCount() {
        return likeCount;
    }

    public void setLikeCount(Integer likeCount) {
        this.likeCount = likeCount;
    }









}
