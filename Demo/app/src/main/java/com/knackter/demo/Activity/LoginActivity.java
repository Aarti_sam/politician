package com.knackter.demo.Activity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.knackter.demo.R;

import butterknife.Bind;
import butterknife.ButterKnife;

public class LoginActivity extends AppCompatActivity {

    private static final String TAG = "LoginActivity";
    private static final int REQUEST_SIGNUP = 0;

    @Bind(R.id.input_email)
    EditText _emailText;
    @Bind(R.id.input_password)
    EditText _passwordText;
    @Bind(R.id.btn_login)
    Button _loginButton;
    @Bind(R.id.link_signup)
    TextView _signupLink;

    SharedPreferences sp;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        ButterKnife.bind(this);

        sp = getSharedPreferences("login", MODE_PRIVATE);

        //if SharedPreferences contains username and password then directly redirect to Home activity
        if (sp.contains("_emailText") && sp.contains("_passwordText")) {
            startActivity(new Intent(LoginActivity.this, MainActivity.class));
            finish();   //finish current activity
        }

        _loginButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                loginCheck();
            }
        });


        _signupLink.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // Start the Signup activity
                Intent intent = new Intent(getApplicationContext(), SignupActivity.class);
                startActivityForResult(intent, REQUEST_SIGNUP);
                finish();
                overridePendingTransition(R.anim.push_left_in, R.anim.push_left_out);
            }
        });

    }

    private void loginCheck() {

        if (_emailText.getText().toString().equals("politician@gmail.com") && _passwordText.getText().toString().equals("password")) {
            SharedPreferences.Editor e = sp.edit();
            e.putString("_emailText", "politician@gmail.com");
            e.putString("_passwordText", "password");
            // SharedPreferences.Editor e = sp.edit();
            e.commit();

            Toast.makeText(LoginActivity.this, "Login Successful", Toast.LENGTH_LONG).show();

            startActivity(new Intent(LoginActivity.this, MainActivity.class));
            finish();
        }else {

            Toast.makeText(LoginActivity.this, "Login Failed!!", Toast.LENGTH_LONG).show();
        }

    }
}
