package com.knackter.demo.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by RUYEE on 2/27/2017.
 */

public class TweetDataResponce {

    public int getPage() {
        return page;
    }

    public void setPage(int page) {
        this.page = page;
    }

    public List<TweetData> getResults() {
        return results;
    }

    public void setResults(List<TweetData> results) {
        this.results = results;
    }

    public int getTotalResults() {
        return totalResults;
    }

    public void setTotalResults(int totalResults) {
        this.totalResults = totalResults;
    }

    public int getTotalPages() {
        return totalPages;
    }

    public void setTotalPages(int totalPages) {
        this.totalPages = totalPages;
    }

    @SerializedName("page")
    private int page;
    @SerializedName("results")
    private List<TweetData> results;
    @SerializedName("total_results")
    private int totalResults;
    @SerializedName("total_pages")
    private int totalPages;
}
