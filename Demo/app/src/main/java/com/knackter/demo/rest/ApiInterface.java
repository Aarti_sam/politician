package com.knackter.demo.rest;

import com.knackter.demo.model.TweetData;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.POST;
import retrofit2.http.Path;
import retrofit2.http.Query;

/**
 * Created by RUYEE on 2/27/2017.
 */

public interface ApiInterface {

    @GET("tweet")
    Call<List<TweetData>> getTweet(@Query("pageNumber") int pageNumber);


   @POST("tweet")
   Call<List<TweetData>> newPost(@Header("Content-Type") String contentType,@Body TweetData tweet);

    /*@DELETE("tweet/{tweetId}")
    Call<List<TweetData>>getTweets(@Path("tweetId") int tweetId);*/


     @DELETE("tweet/{tweetId}")
     Call<Void> deleteItem(@Path("tweetId") int tweetId);
}
