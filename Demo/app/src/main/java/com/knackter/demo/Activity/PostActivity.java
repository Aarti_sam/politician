package com.knackter.demo.Activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.knackter.demo.R;
import com.knackter.demo.model.TweetData;
import com.knackter.demo.rest.ApiInterface;
import com.knackter.demo.rest.Apiclient2;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class PostActivity extends AppCompatActivity {

    private static final String TAG = PostActivity.class.getSimpleName();
    private ApiInterface apiService = Apiclient2.getClient().create(ApiInterface.class);

    String contentType="application/json";
    String content ,imageUrl;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_post);

        final EditText contentEdit = (EditText) findViewById(R.id.postText);
        final EditText imageUrlEdit = (EditText) findViewById(R.id.postText2);
        Button btn1 = (Button) findViewById(R.id.btn_submit);
        Button btn2 = (Button) findViewById(R.id.btn1_submit);

        btn1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                TweetData tweet = new TweetData(
                contentEdit.getText().toString(),
                imageUrlEdit.getText().toString());

                sendNewRequest(tweet);

            }
        });

        btn2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(PostActivity.this, MainActivity.class);
                startActivity(intent);
            }
        });
    }





    //, String imageUrl, ,imageUrl

    private void sendNewRequest(TweetData tweet) {

//contentType
        apiService.newPost(contentType,tweet).enqueue(new Callback<List<TweetData>>()
        {
            @Override
            public void onResponse(Call<List<TweetData>> call, Response<List<TweetData>> response)
            {
                if (response.isSuccessful()){

                    Log.d(TAG, "response code"  + response.body().toString());
                    Log.i(TAG, "post submitted to API." + response.body().toString());
                    Toast.makeText(PostActivity.this , "Post submitted" , Toast.LENGTH_SHORT).show();


                }
            }

            @Override
            public void onFailure(Call<List<TweetData>> call, Throwable t)
            {
                Log.e(TAG, "Unable to submit post to API.");
            }
        });
    }
}
